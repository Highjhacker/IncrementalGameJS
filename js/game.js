var state = {
    money: 0,
    player_total_clicks: 0,
    player_money_per_clicks: 1,
    clickers: [
        new Clicker("Miner", 10, 0.5),
        new Clicker("Worker", 100, 1.5),
        new Clicker("Driller", 1100, 8),
        new Clicker("Jackhammer", 12000, 47)
    ],
    clickers_total: 0,
    achievements: [
      new Achievement(1, "First click", "Click for the first time !", "state.player_total_clicks > 0"),
      new Achievement(2, "A hundred clicks", "Click a hundred time !", "state.player_total_clicks > 100"),
      new Achievement(3, "Shiny Shmeckels", "Gather a hundred dollars from all sources", "state.money > 100"),
      new Achievement(4, "Watch me whip", "Buy 50 clickers", "state.clickers_total > 50"),
      new Achievement(5, "Slap dat b..utton", "Click a thousand time", "state.player_total_clicks > 1000"),
      new Achievement(6, "Millionaire", "Gather a million $", "state.money > 1000000"),
      new Achievement(7, "Billionaire", "Gather a billion $, dayummmm", "state.money > 1000000000"),
      new Achievement(8, "Trumpster", "Buy 100 clickers. TO BUILD A WALL", "state.clickers_total >= 100"),
    ],
    upgrades: [
        new Upgrade(1, "Crank it up", 50, "state.player_money_per_clicks += 1"),
        new Upgrade(2, "Iron Wrist", 100, "state.player_money_per_clicks += 2"),
        new Upgrade(3, "Make the Wall great again", 500, "state.clickers[0].rentability *= 2"),
        new Upgrade(4, "Workers, get to work !", 2500, "state.clickers[1].rentability *= 2"),
        new Upgrade(5, "Drillers killed my vibes", 10000, "state.clickers[2].rentability *= 2"),
    ],
    random_phrases: [
        "One of your miner is a guy with a mustache",
        "It's drill time !",
        "A study indicates that dolphins talk to each other in sentences.",
        "The person who invented the Frisbee was cremated and made into frisbees after he died!",
        "One of your driller fell in love with another worker",
        "Cherophobia is the fear of fun",
        "Human saliva has a boiling point three times that of regular water",
        "Gold and copper were the first metals to be discovered by man around 5,000 BC",
        "Archeologists have unearthed copper drain pipes from as far back as 3,500 BC that are still in good condition",
        "The chemical symbol for gold is Au, from the Latin aurum, which means 'shining dawn'",
        "Petroleum is used to make over 6,000 items, including ink, golf bags, deodorant, footballs, DVDs, crayons & dentures",
        "'The luck of the Irish' is an old minin expression that came out of the gold and silver rush in the second half of the 19th century",
        "After gold has been mined, approx. 63% is used in jewelry 21% as coins, 15% in industrial uses including electronics and the other 1% in dental",
        "Zinc is 100% recyclable. Over 80% of the zinc available for recycling is currently recycled",
        "The Cullinan diamond is the largest gem-quality diamond ever found, weighing roughly 3106.75 carats (1,37 lb)",
        "Most modern electronic devices contain over 35 minerals",
        "69 days is the longest time survived underground after a mining disaster"
    ]
};

function Clicker(name, baseCost, rentability) {
    this.name = name;
    this.baseCost = baseCost;
    this.cost = baseCost;
    this.numberOf = 0;
    this.rentability = rentability;
}

Clicker.prototype.getName = function()
{
    return this.name;
};

Clicker.prototype.getCost = function()
{
    return this.cost;
};

Clicker.prototype.getNumberOf = function()
{
    return this.numberOf;
};

Clicker.prototype.getRentability = function()
{
    return this.rentability;
};

Clicker.prototype.buyX = function(id, amount)
{
    this.cost = Math.floor(this.baseCost * Math.pow(1.25, this.numberOf) * amount);
    if(state.money >= this.cost)
    {
        this.numberOf += amount;
        state.money -= this.cost;
        state.clickers_total += amount;
        updateClickersValues();
        updateRentabilityValues();
        updateTotalClickers();
        updateMoney();
        outputToTextArea("Buyed " + amount + " " + this.name + "(s)");
    }
    else
    {
        console.log("Not enough money, missing: " + (this.cost - state.money));
    }
    document.getElementById(id).innerHTML = this.cost;
};

Clicker.prototype.automateClick = function()
{
    state.money += this.numberOf * this.rentability;
    updateMoney();
};

function Upgrade(index, name, cost, condition) {
    this.index = index;
    this.name = name;
    this.cost = cost;
    this.effect = new Function("return " + condition + ";");
    this.buyed = false;
}

Upgrade.prototype.buy = function() {
    if (state.money >= this.cost) {
        console.log("Upgrade Buyed : " + this.name);
        state.money -= this.cost;
        this.buyed = true;
        document.getElementById("current_money").innerHTML = state.money.toString();
        this.updateValue(this.index);
        this.effect();
        updateRentabilityValues();
        outputToTextArea("Buyed " + this.name + " upgrade");
    }
};

Upgrade.prototype.updateValue = function() {
    var element = document.getElementById("upgrade" + this.index);
    if (this.buyed == true) {
        element.disabled = true;
        element.innerHTML = "BUYED";
    }
    else {
        element.disabled = false;
        element.innerHTML = "Acheter pour " + this.cost + " golds";
    }
};

function Achievement(index, name, text, condition) {
    this.index = index;
    this.achieved = false;
    this.name = name;
    this.text = text;
    this.condition = new Function("return " + condition + ";");
    this.time = undefined
}

Achievement.prototype.checkAchievement = function() {
    if(this.achieved) return true;
    else
    {
        if(this.condition())
        {
            this.achieved = true;
            this.time = new Date();
            console.log("Achieved");
            outputToTextArea("Achievement '" + this.name + "' unlocked the " + this.time.toLocaleDateString() +  " at " + this.time.toLocaleTimeString());
        }
    }
};

Achievement.prototype.updateAchievement = function() {
    this.checkAchievement();
    if(this.achieved) {
        var element = document.getElementById("achiev" + this.index);
        element.innerHTML = "Done : " + this.time.toDateString() + " at " + this.time.toLocaleTimeString();
    }
};

function buttonClick()
{
    state.money += state.player_money_per_clicks;
    state.player_total_clicks += 1;
    console.log("Income of money" + state.money);
    updateMoney();
    updateTotalClicks();
}

function updateMoney()
{
    var element = document.getElementById("current_money");
    element.innerHTML = state.money;
}

function updateTotalClicks()
{
    var element = document.getElementById("current_clicks");
    element.innerHTML = state.player_total_clicks;
}

function updateTotalClickers()
{
    var element = document.getElementById("current_clickers");
    element.innerHTML = state.clickers_total;
}

function updateClickersValues()
{
    document.getElementById("current_miners").innerHTML = state.clickers[0].numberOf;
    document.getElementById("current_workers").innerHTML = state.clickers[1].numberOf;
    document.getElementById("current_drillers").innerHTML = state.clickers[2].numberOf;
    document.getElementById("current_jackhammers").innerHTML = state.clickers[3].numberOf;
}

function updateRentabilityValues()
{

    document.getElementById("minersRentability").innerHTML = state.clickers[0].rentability * state.clickers[0].numberOf;
    document.getElementById("workersRentability").innerHTML = state.clickers[1].rentability * state.clickers[1].numberOf;
    document.getElementById("drillersRentability").innerHTML = state.clickers[2].rentability * state.clickers[2].numberOf;
    document.getElementById("jackhammersRentability").innerHTML = state.clickers[3].rentability * state.clickers[3].numberOf;
}

function checkAutoSaveBox()
{
    if(document.getElementById("auto_save_check").checked == true)
    {
        saveGame();
        console.log("Game auto saved");
    }
}

function saveGame(){
    localStorage.setItem("save", JSON.stringify(state));
    console.log("Game saved !");
    outputToTextArea("Game saved")
}

function loadGame()
{
    var save = localStorage.getItem("save");

    var saveGame = JSON.parse(save);
    state.money = saveGame.money;
    state.player_total_clicks = saveGame.player_total_clicks;
    state.clickers_total = saveGame.clickers_total;
    state.player_money_per_clicks = saveGame.player_money_per_clicks;

    for (var i = 0; i < state.clickers.length; i++)
    {
        state.clickers[i].numberOf = saveGame.clickers[i].numberOf;
        state.clickers[i].rentability = saveGame.clickers[i].rentability;
    }

    for (var j = 0; j < state.upgrades.length; j++)
    {
        if (state.upgrades[j].buyed)
        {
            console.log(state.upgrades[j].name + " buyed before and loaded now");
            state.upgrades[j].buyed = saveGame.upgrades[j].buyed;
            state.upgrades[j].updateValue(state.upgrades[j].index);
        }

    }
    updateRentabilityValues();
    updateMoney();
    updateClickersValues();
    updateTotalClicks();
    updateTotalClickers();
    console.log("LOADED");
    outputToTextArea("Game loaded");
}

function outputToTextArea(phrase)
{
    var element = document.getElementById("exampleTextArea");
    element.spellcheck = false;

    $(element).append(phrase + '\n').text();
    element.scrollTop = element.scrollHeight;
}

function outputRandomPhraseToTextArea()
{
    var element = document.getElementById("exampleTextArea");
    var randPhraseNum = Math.floor(Math.random() * (state.random_phrases.length));
    $(element).append(state.random_phrases[randPhraseNum] + '\n').text();
    element.scrollTop = element.scrollHeight;
}

function clearTextArea()
{
    $(document).ready(function() {
        $("#clearText").click(function(e) {
            e.preventDefault();
            $("#exampleTextArea").val(" ");
            console.log("Cleared the textArea");
        });
    });
}

window.setInterval(function() {
    state.clickers.forEach(function(element) {
        if(element.numberOf != 0)
        {
            element.automateClick();
        }
    });
    state.achievements.forEach(function(element) {
        element.updateAchievement();
    });
}, 1000);

window.setInterval(function() {
    checkAutoSaveBox();
    outputRandomPhraseToTextArea();
}, 60000);